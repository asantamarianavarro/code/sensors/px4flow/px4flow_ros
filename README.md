ROS metapackage for PX4Flow sensor
----------

Notes:

1. The master branch uses catkin. If you wish to use rosbuild, please use the rosbuild branch.
2. Tested on Ubuntu 16.04 and ROS Kinetic.
